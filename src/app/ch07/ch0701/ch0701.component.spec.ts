import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0701Component } from './ch0701.component';

describe('Ch0701Component', () => {
  let component: Ch0701Component;
  let fixture: ComponentFixture<Ch0701Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0701Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0701Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
