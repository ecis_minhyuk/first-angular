import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0701',
  templateUrl: './ch0701.component.html',
  styleUrls: ['./ch0701.component.css']
})
export class Ch0701Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  condition: boolean = true;
  changeCondition = function() {
    this.condition = !this.condition;
  }
  changeDay = function() {
    this.time = 'day';
  }
  changeNight = function() {
    this.time = 'night';
  }
  people: string[] = [
    "Andrew", "Dillon", "Philipe", "Susan"
  ]
  monsters = [
    {
      name: "Nessie",
      location: "Loch Ness, Scotland"
    },
    {
      name: "Bigfoot",
      location: "Pacific Northwest, USA"
    },
    {
      name: "Godzila",
      location: "Tokyo, sometimes New York"
    }
  ]
  time: string = 'night';
}
