import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0702',
  templateUrl: './ch0702.component.html',
  styleUrls: ['./ch0702.component.css']
})
export class Ch0702Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  colors: string[] = [
    "red", "blue", "green", "yellow"
  ];
  name: string;
  color: string = 'color';
  isDisabled: boolean = true;
  classes: string[] = [
    'bold',
    'italic',
    'highlight'
  ];
  selectedClass: string[] = [];
  enabler() {
    this.isDisabled = !this.isDisabled;
  }
  addClass(event:any) {
    this.selectedClass = [];
    var values = event.target.options;
    var opt: any;

    for (var i=0, iLen = values.length; i<iLen; i++) {
      opt = values[i];

      if (opt.selected) {
        this.selectedClass.push(opt.text);
      }
    }
  }
}
