import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0702Component } from './ch0702.component';

describe('Ch0702Component', () => {
  let component: Ch0702Component;
  let fixture: ComponentFixture<Ch0702Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0702Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0702Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
