import { Directive, ElementRef, HostListener, Input, Renderer } from '@angular/core';

@Directive({
  selector: '[appZoom]'
})
export class ZoomDirective {

  constructor(private el: ElementRef, private renderder: Renderer) { }

  @HostListener('mouseenvet') onMouseEnter() {
    this.border('lime', 'solid', '5px');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.border();
  }

  @HostListener('wheel', ['$event']) onWheel(event: any) {
    event.preventDefault();
    if(event.deltaY > 0) {
      this.changeSize(-25);
    }

    if(event.deltaY < 0 ) {
      this.changeSize(25);
    }
  }

  private border(
    color: string = null,
    type: string = null,
    width: string = null
  ) {
    this.renderder.setElementStyle(
      this.el.nativeElement, 'border-color', color
    );
    this.renderder.setElementStyle(
      this.el.nativeElement, 'border-style', type
    );
    this.renderder.setElementStyle(
      this.el.nativeElement, 'border-width', width
    );
  }

  private changeSize(sizechange: any) {
    let height: any = this.el.nativeElement.offsetHeight;
    let newHeight: any = height + sizechange;
    this.renderder.setElementStyle(
      this.el.nativeElement, 'height', newHeight + 'px'
    );
  }
}
