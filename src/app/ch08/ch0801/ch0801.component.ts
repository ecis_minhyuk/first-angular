import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0801',
  templateUrl: './ch0801.component.html',
  styleUrls: ['./ch0801.component.css']
})
export class Ch0801Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  images: string[] = [
    '../assets/images/B001492535.jpg',
    '../assets/images/pp_23979_1.jpg',
    '../assets/images/2325E74C591C611F1E.jpg',
  ]

}
