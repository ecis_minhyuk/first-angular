import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0801Component } from './ch0801.component';

describe('Ch0801Component', () => {
  let component: Ch0801Component;
  let fixture: ComponentFixture<Ch0801Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0801Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0801Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
