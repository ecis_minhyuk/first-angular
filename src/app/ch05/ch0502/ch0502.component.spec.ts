import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0502Component } from './ch0502.component';

describe('Ch0502Component', () => {
  let component: Ch0502Component;
  let fixture: ComponentFixture<Ch0502Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0502Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0502Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
