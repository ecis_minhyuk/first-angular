import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0502',
  templateUrl: './ch0502.component.html',
  template: `
    Directly accessing variables in the component:<br>
    {{speed}} {{vehicle}}<hr>
    adding variables in the component:<br>
    {{ speed + ' ' + vehicle }}<hr>
    Calling function in the component:<br>
    {{ lower(speed) }} {{upper('Jeep')}}<hr>
    <a (click)="setValues('fast', newVehicle)">
      Click to change to Fast </a><hr>
    <a (click)="setValues(newSpeed, 'Rocket')">
      Click to change to Rocket</a><hr>
    <a (click)="vehicle='Car'">
      Click to change the vehicle to a Car</a><hr>
    <a (click)="vehicle='Enhanced ' + vehicle">
      Click to Enhanced Vehicle</a><hr>
  `,
  styles: [`
    a { color: blue; text-decoratin: underline; cursor: pointer; }
  `]
})
export class Ch0502Component implements OnInit {
  speed = 'Slow';
  vehicle = 'Train';
  newSpeed = 'Hypersonic';
  newVehicle = 'Plane';
  upper = function(str: any) {
    str = str.toUpperCase();
    return str;
  }
  lower = function(str: any) {
    return str.toLowerCase();
  }
  setValues = function(speed: any, vehicle: any) {
    this.speed = speed;
    this.vehicle = vehicle;
  }
  constructor() { }

  ngOnInit() {
  }

}
