import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0504',
  templateUrl: './ch0504.component.html',
  styleUrls: ['./ch0504.component.css']
})
export class Ch0504Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  today = Date.now();
  jsonObject = [{title: "mytitle"}, {title: "Programmer"}];
  days = ['Sunday', 'Monday', 'Tuesday', ' Wednesday', 'Thursday', 'Friday', 'Saturday'];
  wait = new Promise<string>( (res, err) => {
    setTimeout(function () {
      res('wati for it...');
    }, 1000);
  });
  dairy = new Promise<string>((res, err) => {
    setTimeout(function () {
      res('dairy');
    }, 2000);
  })
}
