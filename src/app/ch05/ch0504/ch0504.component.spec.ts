import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0504Component } from './ch0504.component';

describe('Ch0504Component', () => {
  let component: Ch0504Component;
  let fixture: ComponentFixture<Ch0504Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0504Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0504Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
