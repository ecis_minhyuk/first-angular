import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0503Component } from './ch0503.component';

describe('Ch0503Component', () => {
  let component: Ch0503Component;
  let fixture: ComponentFixture<Ch0503Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0503Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0503Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
