import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0505Component } from './ch0505.component';

describe('Ch0505Component', () => {
  let component: Ch0505Component;
  let fixture: ComponentFixture<Ch0505Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0505Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0505Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
