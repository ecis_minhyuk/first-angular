import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0505',
  template: `
    {{phrase | censor:"*****"}}
  `,
  styleUrls: ['./ch0505.component.css']
})
export class Ch0505Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

    phrase:string = "This bad phrase is rotten";
}
