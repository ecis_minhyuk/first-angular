import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0501Component } from './ch0501.component';

describe('Ch0501Component', () => {
  let component: Ch0501Component;
  let fixture: ComponentFixture<Ch0501Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0501Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0501Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
