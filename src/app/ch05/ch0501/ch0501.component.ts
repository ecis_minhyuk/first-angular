import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0501',
  template: `
    <h1>Expressions</h1>
    Number:<br>
    5<hr>
    String:<br>
    {{'My String'}}<hr>
    Adding two numbers together:<br>
    {{5+5}}<hr>
    Adding strings and numbers together:<br>
    {{5 + '+' + 5 + '='}}{{5+5}}<hr>
    Comparing tow numbers with each other:<br>
    {{5===5}}<hr>
  `,
  //templateUrl: './ch0501.component.html',
  styleUrls: ['./ch0501.component.css']
})
export class Ch0501Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
