import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0602Component } from './ch0602.component';

describe('Ch0602Component', () => {
  let component: Ch0602Component;
  let fixture: ComponentFixture<Ch0602Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0602Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0602Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
