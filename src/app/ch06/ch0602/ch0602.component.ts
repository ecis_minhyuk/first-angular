import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0602',
  templateUrl: './ch0602.component.html',
  styleUrls: ['./ch0602.component.css']
})
export class Ch0602Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  myPic: string = "../../../assets/images/dahyun.jpg";
  isEnabled: boolean = false;
  className: string = "myClass";
}
