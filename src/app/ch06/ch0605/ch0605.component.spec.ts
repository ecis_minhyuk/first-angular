import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0605Component } from './ch0605.component';

describe('Ch0605Component', () => {
  let component: Ch0605Component;
  let fixture: ComponentFixture<Ch0605Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0605Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0605Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
