import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0605',
  templateUrl: './ch0605.component.html',
  styleUrls: ['./ch0605.component.css']
})
export class Ch0605Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  counter = 0;
  mouse: string;
  upValues: string = '';
  downValues: string = '';
  keypressValue: string = '';
  x: string = '';
  y: string = '';
  view: string = '';

  mouseGoesIn = function() {
    this.mouse = "entered";
  }
  mouseLeft = function() {
    this.mouse = "left";
  }

  imageArray: string[] = [
    "asd",
    "a",
    "a"
  ]
  imageUrl: string = this.imageArray[this.counter];

  changeImg = function() {
    if(this.counter < this.imageArray.length -1) {
      this.counter++;
    } else {
      this.counter = 0;
    }
    this.imageUrl = this.imgArray[this.counter];
  }

  onKeyup(event: any) {
    this.upValues = event.key
  }
  onKeydown(event: any) {
    this.downValues = event.key
  }
  keypress(event:any){
    this.keypressValue = event.key;
  }
  move(event:any) {
    this.x = event.clientX;
    this.y = event.clientY;
  }

  underTheScope(event:any) {
    if(event.type == "focus") {
      this.view = "the text box is focused";
    } else if(event.type == "blur") {
      this.view = "the input box is blurred";
    }
    console.log( event );
  }
}
