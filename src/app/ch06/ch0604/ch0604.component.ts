import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0604',
  templateUrl: './ch0604.component.html',
  styleUrls: ['./ch0604.component.css']
})
export class Ch0604Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  twoColors: boolean = true;
  changeColor = function() {
    this.twoColors = !this.twoColors;
  }
  myBorder = "1px solid black"
}
