import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0604Component } from './ch0604.component';

describe('Ch0604Component', () => {
  let component: Ch0604Component;
  let fixture: ComponentFixture<Ch0604Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0604Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0604Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
