import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0603',
  templateUrl: './ch0603.component.html',
  styleUrls: ['./ch0603.component.css']
})
export class Ch0603Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  myCustomClass: string = 'blueBox';
  isTrue = true;
}
