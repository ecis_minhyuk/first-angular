import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0603Component } from './ch0603.component';

describe('Ch0603Component', () => {
  let component: Ch0603Component;
  let fixture: ComponentFixture<Ch0603Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0603Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0603Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
