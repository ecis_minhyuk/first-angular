import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0601',
  templateUrl: './ch0601.component.html',
  styleUrls: ['./ch0601.component.css']
})
export class Ch0601Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  str1: string = "Hello my name is"
  name: string = "Brendan"
  str2: string = "I like to"
  likes: string[] = ['hike', "rappel", "Jeep"]
  getLikes = function(arr: any) {
    var arrString = arr.join(", ");
    return " " + arrString
  }
  imageSrc: string = "../../../assets/images/dahyun.jpg"
}
