import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0601Component } from './ch0601.component';

describe('Ch0601Component', () => {
  let component: Ch0601Component;
  let fixture: ComponentFixture<Ch0601Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0601Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0601Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
