import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0606Component } from './ch0606.component';

describe('Ch0606Component', () => {
  let component: Ch0606Component;
  let fixture: ComponentFixture<Ch0606Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0606Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0606Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
