import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0405inputComponent } from './ch0405input.component';

describe('Ch0405inputComponent', () => {
  let component: Ch0405inputComponent;
  let fixture: ComponentFixture<Ch0405inputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0405inputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0405inputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
