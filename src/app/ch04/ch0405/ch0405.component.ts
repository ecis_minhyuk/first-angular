import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0405',
  template: `
    <myInput name="Brendan" occupation="Student/Author"></myInput>
    <myInput name="Brad" occupation="Analyst/Author"></myInput>
    <myInput name="Caleb" occupation="Student/Author"></myInput>
  `,
  styleUrls: ['./ch0405.component.css']
})
export class Ch0405Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  title = 'Using Inputs in Angular'
}
