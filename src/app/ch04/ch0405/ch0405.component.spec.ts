import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0405Component } from './ch0405.component';

describe('Ch0405Component', () => {
  let component: Ch0405Component;
  let fixture: ComponentFixture<Ch0405Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0405Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0405Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
