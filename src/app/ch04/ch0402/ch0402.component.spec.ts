import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0402Component } from './ch0402.component';

describe('Ch0402Component', () => {
  let component: Ch0402Component;
  let fixture: ComponentFixture<Ch0402Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0402Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0402Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
