import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0402',
  template: `
    <p>Hello today is {{today}}!</p>
  `,
  styleUrls: ['./ch0402.component.css']
})
export class Ch0402Component implements OnInit {
  today: Date
  constructor() {
    this.today = new Date();
   }

  ngOnInit() {
  }

}
