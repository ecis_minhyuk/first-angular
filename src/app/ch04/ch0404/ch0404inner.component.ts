import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nested',
  template: `
    <span>Congratulations I'm a nested component</span>
  `,
  styles: [`
    span {
      color: #228b22;
    }
  `]
})
export class Ch0404innerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
