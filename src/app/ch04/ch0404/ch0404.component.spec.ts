import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0404Component } from './ch0404.component';

describe('Ch0404Component', () => {
  let component: Ch0404Component;
  let fixture: ComponentFixture<Ch0404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
