import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0404innerComponent } from './ch0404inner.component';

describe('Ch0404innerComponent', () => {
  let component: Ch0404innerComponent;
  let fixture: ComponentFixture<Ch0404innerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0404innerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0404innerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
