import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0403',
  templateUrl: './ch0403.component.html',
  styleUrls: ['./ch0403.component.css']
})
export class Ch0403Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  title = 'Chapter 4 Using External templates and styles';
}
