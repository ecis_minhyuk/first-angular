import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0403Component } from './ch0403.component';

describe('Ch0403Component', () => {
  let component: Ch0403Component;
  let fixture: ComponentFixture<Ch0403Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0403Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0403Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
