import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ch0401',
  template: `
    <span>Hello my name is Brendan</span>
  `,
  styles: [`
    span {
      font-weight:bold;
      border:1px ridge blue;
      padding:5px;
    }
  `]
})
export class Ch0401Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  title = 'Chapter 4 Intro'
}