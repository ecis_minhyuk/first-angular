import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch0401Component } from './ch0401.component';

describe('Ch0401Component', () => {
  let component: Ch0401Component;
  let fixture: ComponentFixture<Ch0401Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch0401Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch0401Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
