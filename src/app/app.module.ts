import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingAllModule} from './routing-all/routing-all.module'
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { Ch03Component } from './ch03/ch03.component';
import { Ch0401Component } from './ch04/ch0401/ch0401.component';
import { Ch0402Component } from './ch04/ch0402/ch0402.component';
import { Ch0403Component } from './ch04/ch0403/ch0403.component';
import { Ch0404Component } from './ch04/ch0404/ch0404.component';
import { Ch0404innerComponent } from './ch04/ch0404/ch0404inner.component';
import { Ch0405Component } from './ch04//ch0405/ch0405.component';
import { Ch0405inputComponent } from './ch04/ch0405/ch0405input.component';
import { Ch0501Component } from './ch05/ch0501/ch0501.component';
import { Ch0502Component } from './ch05/ch0502/ch0502.component';
import { Ch0503Component } from './ch05/ch0503/ch0503.component';
import { Ch0504Component } from './ch05/ch0504/ch0504.component';
import { Ch0505Component } from './ch05/ch0505/ch0505.component';
import { CensorPipe } from './ch05/ch0505/censor.pipe';
import { Ch0601Component } from './ch06/ch0601/ch0601.component';
import { Ch0602Component } from './ch06/ch0602/ch0602.component';
import { Ch0603Component } from './ch06/ch0603/ch0603.component';
import { Ch0604Component } from './ch06/ch0604/ch0604.component';
import { Ch0605Component } from './ch06/ch0605/ch0605.component';
import { Ch0606Component } from './ch06/ch0606/ch0606.component';
import { Ch0701Component } from './ch07/ch0701/ch0701.component';
import { Ch0702Component } from './ch07/ch0702/ch0702.component';
import { Ch0801Component } from './ch08/ch0801/ch0801.component';
import { ZoomDirective } from './ch08/ch0801/zoom.directive';


@NgModule({
  declarations: [
    AppComponent,
    Ch03Component,
    HomeComponent,
    Ch0401Component,
    Ch0402Component,
    Ch0403Component,
    Ch0404Component,
    Ch0404innerComponent,
    Ch0405Component,
    Ch0405inputComponent,
    Ch0501Component,
    Ch0502Component,
    Ch0503Component,
    Ch0504Component,
    Ch0505Component,
    CensorPipe,
    Ch0601Component,
    Ch0602Component,
    Ch0603Component,
    Ch0604Component,
    Ch0605Component,
    Ch0606Component,
    Ch0701Component,
    Ch0702Component,
    Ch0801Component,
    ZoomDirective,
  ],
  imports: [ 
    BrowserModule,
    HttpModule,
    FormsModule,
    RoutingAllModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
