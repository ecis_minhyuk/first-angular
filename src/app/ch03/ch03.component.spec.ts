import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ch03Component } from './ch03.component';

describe('Ch03Component', () => {
  let component: Ch03Component;
  let fixture: ComponentFixture<Ch03Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ch03Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ch03Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
