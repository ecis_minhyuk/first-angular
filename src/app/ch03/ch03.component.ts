import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'message',
  template: `
  <h1>Hello World!</h1>
  `,
  styleUrls: ['./ch03.component.css']
})
export class Ch03Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  //title = 'My First Angular App';
  title = 'Chapter 3 Example';
}
