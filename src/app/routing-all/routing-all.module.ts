import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { Ch03Component } from '../ch03/ch03.component';
import { Ch0401Component } from '../ch04/ch0401/ch0401.component';
import { Ch0402Component } from '../ch04/ch0402/ch0402.component';
import { Ch0403Component } from '../ch04/ch0403/ch0403.component';
import { Ch0404Component } from '../ch04/ch0404/ch0404.component';
import { Ch0405Component } from '../ch04/ch0405/ch0405.component';
import { Ch0501Component } from '../ch05/ch0501/ch0501.component';
import { Ch0502Component } from '../ch05/ch0502/ch0502.component';
import { Ch0503Component } from '../ch05/ch0503/ch0503.component';
import { Ch0504Component } from '../ch05/ch0504/ch0504.component';
import { Ch0505Component } from '../ch05/ch0505/ch0505.component';
import { Ch0601Component } from '../ch06/ch0601/ch0601.component';
import { Ch0602Component } from '../ch06/ch0602/ch0602.component';
import { Ch0603Component } from '../ch06/ch0603/ch0603.component';
import { Ch0604Component } from '../ch06/ch0604/ch0604.component';
import { Ch0605Component } from '../ch06/ch0605/ch0605.component';
import { Ch0606Component } from '../ch06/ch0606/ch0606.component';
import { Ch0701Component } from '../ch07/ch0701/ch0701.component';
import { Ch0702Component } from '../ch07/ch0702/ch0702.component';
import { Ch0801Component } from '../ch08/ch0801/ch0801.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'ch03',
    component: Ch03Component
  },
  {
    path:'ch04',
    component: Ch0401Component
  },
  {
    path:'ch0401',
    component: Ch0401Component
  },
  {
    path:'ch0402',
    component: Ch0402Component
  },
  {
    path:'ch0403',
    component: Ch0403Component
  },
  {
    path:'ch0404',
    component: Ch0404Component
  },
  {
    path:'ch0405',
    component: Ch0405Component
  },
  {
    path:'ch0501',
    component: Ch0501Component
  },
  {
    path:'ch0502',
    component: Ch0502Component
  },
  {
    path:'ch0503',
    component: Ch0503Component
  },
  {
    path:'ch0504',
    component: Ch0504Component
  },
  {
    path:'ch0505',
    component: Ch0505Component
  },
  {
    path:'ch0601',
    component: Ch0601Component
  },
  {
    path:'ch0602',
    component: Ch0602Component
  },
  {
    path:'ch0603',
    component: Ch0603Component
  },
  {
    path:'ch0604',
    component: Ch0604Component
  },
  {
    path:'ch0605',
    component: Ch0605Component
  },
  {
    path:'ch0606',
    component: Ch0606Component
  },
  {
    path:'ch0701',
    component: Ch0701Component
  },
  {
    path:'ch0702',
    component: Ch0702Component
  },
  {
    path:'ch0801',
    component: Ch0801Component
  }
]


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingAllModule { }
