import { RoutingAllModule } from './routing-all.module';

describe('RoutingAllModule', () => {
  let routingAllModule: RoutingAllModule;

  beforeEach(() => {
    routingAllModule = new RoutingAllModule();
  });

  it('should create an instance', () => {
    expect(routingAllModule).toBeTruthy();
  });
});
